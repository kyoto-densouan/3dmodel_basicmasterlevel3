# README #

1/3スケールの日立 ベーシックマスターLEVEL3風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- 日立製作所

## 発売時期
- ベーシックマスターレベル3 1980年5月
- ベーシックマスターレベル3MarkII 1982年4月
- ベーシックマスターレベル3Mark5 1983年5月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%99%E3%83%BC%E3%82%B7%E3%83%83%E3%82%AF%E3%83%9E%E3%82%B9%E3%82%BF%E3%83%BC)
- [IPSJ コンピュータ博物館](http://museum.ipsj.or.jp/computer/personal/0006.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_basicmasterlevel3/raw/ed15e285bd3612966b9c1becf4fa9c88ac5272b3/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_basicmasterlevel3/raw/ed15e285bd3612966b9c1becf4fa9c88ac5272b3/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_basicmasterlevel3/raw/ed15e285bd3612966b9c1becf4fa9c88ac5272b3/ExampleImage.png)

